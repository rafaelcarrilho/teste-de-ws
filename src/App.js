import React from 'react';
import Header from './Components/Header/Header';
import MainComponent from './Components/MainComponent/MainComponent';

// const cableApp = actioncable.createConsumer('ws://localhost:3000/cable_meetings')

function App() {
  

  return (
    <div >
      <Header/>
      <MainComponent/>
    </div>
  );
}

export default App;
