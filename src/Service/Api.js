import axios from 'axios'

const Api = axios.create({
  baseURL: "https://db-svm.herokuapp.com/",
  headers: {
    "Authorization": "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJleHAiOjE2MTQyODk0NzN9.TJ7C9Iif-RNEnoEqLAnYx21zUwskjCmLd3im4neDoNw",
    "Content-Type": "application/json"
  }
})

export default Api