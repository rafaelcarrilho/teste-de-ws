import React from 'react'
import FormView from '../FormView/FormView'
import ListMeetings from '../ListMeetings/ListMeetings'
import actioncable from 'actioncable'
import ListTasks from '../ListMeetings/ListTasks'

const cableApp = actioncable.createConsumer('wss://db-svm.herokuapp.com/cable')

const MainComponent = (props) => {


  return (
    <div className = "containerMain">
      <ListMeetings cable = {cableApp} />
      <ListTasks cable = {cableApp} />
      <FormView/>
    </div>


  )
}

export default MainComponent