import actioncable from 'actioncable'
import React, { useState, useEffect } from 'react'
import Api from '../../Service/Api'
import WebSocketComponent from './WebSocketComponent'
import './styles/listmeetings.css'
import ButtonsView from './ButtonDiv'

  
const ListMeetings = (props) =>{

  const [meetings, setmeetings] = useState([])
  const updateAppState = (meets) => {
    
    setmeetings(meets)
  }

  useEffect(() => {
    Api.get("/meetings")
    .then(resp => {
      setmeetings(resp.data)
    })
  }, [])

  
  return (
    <div className="list">
      
      <WebSocketComponent 
        cable={props.cable}
        room="meetings"
        updateApp={updateAppState}
      />

      {meetings.map(meet => (
        <div key = {meet.id} id={meet.id} className={meet.state + " listItem"}>
          <h3>{meet.name}</h3>
          <p>{meet.subject}</p>
          <p>prioridade = {meet.priority}</p>
          <ButtonsView state = {meet.state}/>
        </div>
      ))}
    </div>
  )
}
export default ListMeetings