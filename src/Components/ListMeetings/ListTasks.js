import actioncable from 'actioncable'
import React, { useState, useEffect } from 'react'
import Api from '../../Service/Api'
import WebSocketComponent from './WebSocketComponent'
import './styles/listmeetings.css'
import ButtonsView from './ButtonDiv'


  
const ListTasks = (props) =>{

  const [tasks, settasks] = useState([])
  const updateAppState = (new_tasks) => {    
    settasks(new_tasks)
  }

  useEffect(() => {
    Api.get("/tasks")
    .then(resp => {
      settasks(resp.data)
    })
  }, [])

  
  return (
    <div className="listTasks">
      
      <WebSocketComponent 
        cable={props.cable}
        room= "tasks"
        updateApp={updateAppState}
      />

      {tasks.map(task => (
        <div key = {task.id} id={task.id} className={"listItem"}>
          <h3>{task.description}</h3>
          <p>{task.date_limit}</p>
          <p>prioridade = {task.priority}</p>
          {/* <ButtonsView state = {task.state}/> */}
        </div>
      ))}
    </div>
  )
}
export default ListTasks