import React from 'react'
import Api from '../../Service/Api'


const ButtonsView = (props) => {
  const iniciateMeeting = e => {
    Api.put('meetings/'+e.target.parentNode.id, {
      meeting:{
        "state":"Andamento"
      }
    })
  }
  
  const closeMeeting = e => {
    Api.put('meetings/'+e.target.parentNode.id, {
      meeting:{
        "state":"Concluida"
      }
    })
  }

  const reopenMeeting = e => {
    Api.put('meetings/'+e.target.parentNode.id, {
      meeting:{
        "state":"Aguardando"
      }
    })
  }

  const cancelMeeting = e => {
    Api.put ('meetings/'+e.target.parentNode.parentNode.id, {
      meeting:{
        "state":"Cancelada"
      }
    })
  }
  const letItIn = e => {
    Api.put ('meetings/'+e.target.parentNode.parentNode.id, {
      meeting:{
        "state":"Entrar"
      }
    })
  }

  switch (props.state) {
    case "Entrar":
      return (
        <button onClick ={iniciateMeeting}>Iniciar</button>
      );
    case "Andamento":
      return (
        <button onClick ={closeMeeting}>Concluir reunião</button>
      );
    case "Cancelada":
      return(
        <button onClick ={reopenMeeting}>Voltar para a fila</button>
      )
    case "Concluida":
      return null
    default:
      return (
        <div>
          <button onClick={cancelMeeting}>Cancelar</button>
          <button onClick={letItIn}>Entrar</button>
        </div>  
      )
  }
}

export default ButtonsView