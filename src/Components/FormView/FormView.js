import React, { useState } from 'react'
import Api from '../../Service/Api'

const FormView = (props) => {
  const [name, setname] = useState()
  const [previsted_time, setprevisted_time] = useState()
  const [subject, setsubject] = useState()
  const [priority, setpriority] = useState()
  const [companions, setcompanions] = useState()
  const [description, setdescription] = useState()
  const [date_limit, setdate_limit] = useState()
  const [prioritytask, setprioritytask] = useState()

  const handlerSubmit = e => {
    e.preventDefault()
    Api.post("/meetings", {
      "meeting":{
        "name":name,
        "subject":subject,
        "previsted_time":previsted_time,
        "priority": priority,
        "companions":companions
      }
    }).then(() => alert("Criado"))
  }
  
  const handlerTaskSubmit = e => {
    e.preventDefault()
    Api.post("/tasks", {
      "task":{
        "description":description,
        "limit":date_limit,
        "priority": prioritytask
      }
    }).then(() => alert("Criado"))
  }

  const deleteAll = () => {
    Api.delete("meetings/all")
  }

  return(
    <div className= 'formView'>
      <br/><br/>
      <h1>Reuniões</h1>
      <form className="formClass" onSubmit={handlerSubmit}>
        <div>
          <label>Nome completo</label>
          <input value = {name} onChange={e=>setname(e.target.value)}/>
        </div>
        <div>
          <label>Tempo Previsto</label>
          <input value = {previsted_time} onChange={e=>setprevisted_time(e.target.value)}/>
        </div>
        <div>
          <label>Assunto</label>
          <input value = {subject} onChange={e=>setsubject(e.target.value)}/>
        </div>
        <div>
          <label>Nivel de prioridade</label>
          <input value = {priority} onChange={e=>setpriority(e.target.value)}/>
        </div>
        <div>
          <label>Numero de companheiros</label>
          <input value = {companions} onChange={e=>setcompanions(e.target.value)}/>
        </div>
        <input type="submit" value="Enviar nova reunião"/>
      </form>
      <br/><br/>
      <h1>Tarefas</h1>
      <form className="formClass" onSubmit={handlerTaskSubmit}>
        <div>
          <label>Descrição</label>
          <input value = {description} onChange={e=>setdescription(e.target.value)}/>
        </div>
        <div>
          <label>Data Limite</label>
          <input value = {date_limit} onChange={e=>setdate_limit(e.target.value)}/>
        </div>
        <div>
          <label>Prioridade</label>
          <input value = {prioritytask} onChange={e=>setprioritytask(e.target.value)}/>
        </div>
        <input type="submit" value="Enviar nova tarefa"/>
      </form>
      <button onClick={deleteAll}>Deletar todas as reuniões</button>
    </div>
  )
}

export default FormView